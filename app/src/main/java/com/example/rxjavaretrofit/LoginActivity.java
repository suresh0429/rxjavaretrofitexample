package com.example.rxjavaretrofit;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.rxjavaretrofit.Retrofit.MyApi;
import com.example.rxjavaretrofit.Retrofit.RetrofitClient;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Retrofit;

public class LoginActivity extends AppCompatActivity {
    private static final String TAG = "MainActivity";
    MyApi api;
    CompositeDisposable compositeDisposable = new CompositeDisposable();
    @BindView(R.id.etMobile)
    EditText etMobile;
    @BindView(R.id.etpass)
    EditText etpass;
    @BindView(R.id.login)
    Button login;
    @BindView(R.id.progressBar2)
    ProgressBar progressBar2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        // Init API
        Retrofit retrofit = RetrofitClient.getInstance();
        api = retrofit.create(MyApi.class);
    }

    @OnClick(R.id.login)
    public void onViewClicked() {
        progressBar2.setVisibility(View.VISIBLE);
        compositeDisposable.add(api.userLogin(etMobile.getText().toString(), etpass.getText().toString())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(posts -> {

                    progressBar2.setVisibility(View.GONE);

                    if (posts.getStatus()==1){
                        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                    }else {
                        Toast.makeText(this, posts.getMessage(), Toast.LENGTH_SHORT).show();
                    }

                }));


    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        compositeDisposable.clear();

    }
}
