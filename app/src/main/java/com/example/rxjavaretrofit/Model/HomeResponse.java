package com.example.rxjavaretrofit.Model;

import java.util.List;

public class HomeResponse {


    /**
     * status : 1
     * Home_Banners : [{"id":"98","image":"https://pickany24x7.com/image//223871183_mobile2.1.jpg","category":"offers","image_type":"banner"},{"id":"19","image":"https://pickany24x7.com/image//83703566-car-and-bike-banner.jpg","category":"home","image_type":"banner"},{"id":"18","image":"https://pickany24x7.com/image//77518237-mobile5.jpg","category":"home","image_type":"banner"},{"id":"95","image":"https://pickany24x7.com/image//988333466_stationary.jpg","category":"home","image_type":"banner"},{"id":"96","image":"https://pickany24x7.com/image//25453290-celebrations-offers.jpg","category":"offers","image_type":"banner"},{"id":"97","image":"https://pickany24x7.com/image//978204313_stationary-offer.jpg","category":"offers","image_type":"banner"}]
     * Modules : [{"id":"11","image":"https://pickany24x7.com/image/modules/4683140-car_&_bike_bg1.jpg","category":"Car & Bike Rentals","position":"1","title":"Car & Bike Rentals","status":"1"},{"id":"3","image":"https://pickany24x7.com/image/modules/1060000-celebrations_bg2.jpg","category":"Celebrations","position":"2","title":"Celebrations","status":"1"},{"id":"4","image":"https://pickany24x7.com/image/modules/85552368-homekitchen_bg2.jpg","category":"Home Kitchen","position":"3","title":"Home Kitchen","status":"1"},{"id":"5","image":"https://pickany24x7.com/image/modules/21066782-Mobiles3.jpg","category":"Mobiles","position":"4","title":"Mobiles","status":"1"},{"id":"6","image":"https://pickany24x7.com/image/modules/31213834-stationery_bg.jpg","category":"Stationery","position":"5","title":"Stationery","status":"1"},{"id":"1","image":"https://pickany24x7.com/image/modules/23579335-grocery_bg1.jpg","category":"Grocery","position":"6","title":"Grocery","status":"0"},{"id":"2","image":"https://pickany24x7.com/image/modules/67879101-bath-&-body.jpg","category":"Bath & Body","position":"7","title":"Bath & Body","status":"0"},{"id":"8","image":"https://pickany24x7.com/image/modules/30311073-hotels_bg1.jpg","category":"Hotels","position":"8","title":"Hotels","status":"0"},{"id":"10","image":"https://pickany24x7.com/image/modules/63810666-services_bg1.jpg","category":"Services","position":"9","title":"Services","status":"0"},{"id":"9","image":"https://pickany24x7.com/image/modules/46871311-electronic_accessories_bg1.jpg","category":"Electronic  Accessories","position":"10","title":"Electronic  Accessories","status":"0"}]
     * message : Get All Home_Banners & Modules
     */

    private int status;
    private String message;
    private List<HomeBannersBean> Home_Banners;
    private List<ModulesBean> Modules;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<HomeBannersBean> getHome_Banners() {
        return Home_Banners;
    }

    public void setHome_Banners(List<HomeBannersBean> Home_Banners) {
        this.Home_Banners = Home_Banners;
    }

    public List<ModulesBean> getModules() {
        return Modules;
    }

    public void setModules(List<ModulesBean> Modules) {
        this.Modules = Modules;
    }

    public static class HomeBannersBean {
        /**
         * id : 98
         * image : https://pickany24x7.com/image//223871183_mobile2.1.jpg
         * category : offers
         * image_type : banner
         */

        private String id;
        private String image;
        private String category;
        private String image_type;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getCategory() {
            return category;
        }

        public void setCategory(String category) {
            this.category = category;
        }

        public String getImage_type() {
            return image_type;
        }

        public void setImage_type(String image_type) {
            this.image_type = image_type;
        }
    }

    public static class ModulesBean {
        /**
         * id : 11
         * image : https://pickany24x7.com/image/modules/4683140-car_&_bike_bg1.jpg
         * category : Car & Bike Rentals
         * position : 1
         * title : Car & Bike Rentals
         * status : 1
         */

        private String id;
        private String image;
        private String category;
        private String position;
        private String title;
        private String status;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getCategory() {
            return category;
        }

        public void setCategory(String category) {
            this.category = category;
        }

        public String getPosition() {
            return position;
        }

        public void setPosition(String position) {
            this.position = position;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }
    }
}
