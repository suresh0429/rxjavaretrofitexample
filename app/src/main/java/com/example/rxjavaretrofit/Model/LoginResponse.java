package com.example.rxjavaretrofit.Model;

import java.util.List;

public class LoginResponse {


    /**
     * status : 1
     * userDetails : [{"id":"113","username":"suresh","email":"ksuresh.unique@gmail.com","mobile":"8919480920","ency_password":"0487cc982f7db39c51695026e4bdc692","profile_pic":"https://pickany24x7.com/image/users/USER12694100771545111301674.png"}]
     * message : Login Successfull
     */

    private int status;
    private String message;
    private List<UserDetailsBean> userDetails;



    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<UserDetailsBean> getUserDetails() {
        return userDetails;
    }

    public void setUserDetails(List<UserDetailsBean> userDetails) {
        this.userDetails = userDetails;
    }

    public static class UserDetailsBean {
        /**
         * id : 113
         * username : suresh
         * email : ksuresh.unique@gmail.com
         * mobile : 8919480920
         * ency_password : 0487cc982f7db39c51695026e4bdc692
         * profile_pic : https://pickany24x7.com/image/users/USER12694100771545111301674.png
         */

        private String id;
        private String username;
        private String email;
        private String mobile;
        private String ency_password;
        private String profile_pic;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getMobile() {
            return mobile;
        }

        public void setMobile(String mobile) {
            this.mobile = mobile;
        }

        public String getEncy_password() {
            return ency_password;
        }

        public void setEncy_password(String ency_password) {
            this.ency_password = ency_password;
        }

        public String getProfile_pic() {
            return profile_pic;
        }

        public void setProfile_pic(String profile_pic) {
            this.profile_pic = profile_pic;
        }
    }
}
