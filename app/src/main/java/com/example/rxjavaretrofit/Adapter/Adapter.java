package com.example.rxjavaretrofit.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.rxjavaretrofit.Model.HomeResponse;
import com.example.rxjavaretrofit.Model.Post;
import com.example.rxjavaretrofit.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class Adapter extends RecyclerView.Adapter<Adapter.ViewHolder> {


    private Context context;
    private List<HomeResponse.ModulesBean> notesList;

    public Adapter(Context context, List<HomeResponse.ModulesBean> notesList) {
        this.context = context;
        this.notesList = notesList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        HomeResponse.ModulesBean post = notesList.get(position);

        holder.textOne.setText(post.getCategory());
        holder.textTwo.setText(post.getTitle());
        holder.textThree.setText(""+post.getImage());


    }

    @Override
    public int getItemCount() {
        return notesList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.textOne)
        TextView textOne;
        @BindView(R.id.textTwo)
        TextView textTwo;
        @BindView(R.id.textThree)
        TextView textThree;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }


    }
}
