package com.example.rxjavaretrofit.Retrofit;

import com.example.rxjavaretrofit.Model.HomeResponse;
import com.example.rxjavaretrofit.Model.LoginResponse;
import com.example.rxjavaretrofit.Model.Post;

import java.util.List;


import io.reactivex.Observable;
import io.reactivex.SingleObserver;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface MyApi {
    @GET("h_banners_modules")
    Observable<HomeResponse> getAllPost();

    @FormUrlEncoded
    @POST("user_login")
    Observable<LoginResponse> userLogin(@Field("mobile") String mobile, @Field("password") String password);

}
