package com.example.rxjavaretrofit;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.rxjavaretrofit.Adapter.Adapter;
import com.example.rxjavaretrofit.Model.HomeResponse;
import com.example.rxjavaretrofit.Retrofit.MyApi;
import com.example.rxjavaretrofit.Retrofit.RetrofitClient;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Retrofit;

public class MainActivity extends AppCompatActivity {
    List<HomeResponse.ModulesBean> daat;
    private static final String TAG = "MainActivity";
    MyApi api;
    CompositeDisposable compositeDisposable = new CompositeDisposable();
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        // Init API
        Retrofit retrofit = RetrofitClient.getInstance();
        api = retrofit.create(MyApi.class);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        fetchDataList();

    }

    private void fetchDataList() {
        progressBar.setVisibility(View.VISIBLE);
        compositeDisposable.add(api.getAllPost()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(posts -> {

                    progressBar.setVisibility(View.GONE);

                    Log.d(TAG, "accept: " + posts);

                   /* for (HomeResponse homeResponse : posts.){

                        Log.d(TAG, "fetchDataList: "+homeResponse.getModules());

                        daat = homeResponse.getModules();
                        List<HomeResponse.HomeBannersBean> daat1 = homeResponse.getHome_Banners();
                    }



*/
                    Adapter adapter = new Adapter(MainActivity.this, posts.getModules());
                    recyclerView.setAdapter(adapter);
                }));
    }

    @Override
    protected void onStop() {
        super.onStop();
        compositeDisposable.clear();
    }
}
